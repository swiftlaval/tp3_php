<?php if (isset($_GET['source'])) die(highlight_file(__FILE__, 1)); ?>
<?php
/**
 * Created by PhpStorm.
 * Date: 03-Aug-18
 * Time: 7:42 PM
 */

$conn = mysqli_connect($db_host, $db_user, $db_password);


if (!$conn) {
    die("probleme de connection " . mysqli_error($conn));
}


mysqli_select_db($conn, $db_name) or die("probleme de selection " . mysqli_error($conn));


function show_players($conn)
{

    global $br;
    echo "Liste des joueurs:" . $br;
    $result = mysqli_query($conn, "select * from persons;");
    echo "<table id='playerlist' border='border'></table>";
    $array = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $jsonRow = json_encode($array);
    echo "<script>fillTable($jsonRow,'playerlist');</script>" . $br;


}

function show_terrains_today($conn)
{
    global $br;
    echo "Liste des terrains réservé aujourdhui:" . $br;
    $result = mysqli_query($conn, "select * from reservations where DATE(R_date)=CURRENT_DATE(); ");
    echo "<table id='terrainlist' border='border'></table>";
    $array = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $jsonRow = json_encode($array);
    echo "<script>fillTable($jsonRow,'terrainlist');</script>" . $br;


}

function show_player_reservations($conn, $playerlogin)
{
    global $br;

    $list = get_player_reservations($conn, $playerlogin);
    echo "<table id='playerterrainlist' border='border'></table>";
    echo "<script>let liste=$list;
    liste.unshift({'R_Id':'ID de reservation','P_Id':'Votre ID','T_Id':'ID de terrain','R_date':'Date','supprimer':'Supprimer'});
    fillTableCustom(liste, 'playerterrainlist');</script>" . $br;
}

function get_all_reservations($conn)//retourne liste des reservations en format JSON
{
    $result = mysqli_query($conn, "select * from reservations;");
    $array = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return json_encode($array);

}

function remove_resrvation($conn, $rid)
{
    return mysqli_query($conn, "delete from reservations where R_Id='$rid'; ");
}

function get_player_reservations($conn, $playerlogin)//retourne liste des reservations en format JSON
{

    $p_id = get_player_id_from_login($conn, $playerlogin);
    $result = mysqli_query($conn, "select * from reservations where P_Id='$p_id';");
    $array = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return json_encode($array);

}

function get_player_id_from_login($conn, $playerlogin)
{
    $result = mysqli_query($conn, "select * from persons where Login= '$playerlogin';");
    $array = mysqli_fetch_assoc($result);
    return $array['P_Id'];
}

function add_player($conn, $nom, $prenom, $login, $mot_de_passe, $admin)
{
    $admin ? $adminbool = 1 : $adminbool = 0;
    $result = mysqli_query($conn, "select count(*) from persons where Prenom= '$login';");
    $counter = mysqli_fetch_array($result)["count(*)"];
    if ($counter >= 1) {
        return false;
    } else {
        $query = "INSERT INTO persons (P_Id,Nom, Prenom, Login, Mot_de_passe, Admin )
        VALUES ( NULL ,'$nom','$prenom','$login','$mot_de_passe', '$adminbool')";
        if (mysqli_query($conn, $query)) {
            return true;
        } else {
            return false;
        }

    }

}

function add_reservation($conn, $idjoueur, $idterrain, $date, $time)
{

    $fulldate = $date . ' ' . $time . ':00';
    $result = mysqli_query($conn, "select count(*) from reservations where (T_Id = '$idterrain' and R_date = '$fulldate') or (P_Id ='$idjoueur' and DATE(R_date) = '$date') or DATE_ADD(CURRENT_DATE(),INTERVAL 1 DAY)!= '$date';");
    $counter = mysqli_fetch_array($result)["count(*)"];
    if ($counter >= 1) {
        return 0;
    } else {
        $query = "INSERT INTO reservations (R_Id,P_Id,T_Id,R_date)
        VALUES (NULL ,'$idjoueur', '$idterrain',STR_TO_DATE('$fulldate','%Y-%m-%d %H:%i:%s') )";
        if (mysqli_query($conn, $query)) {
            return 1;
        } else {
            return -1;
        }

    }

}

?>