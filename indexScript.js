function updateIntervalInfo(input) {
    table=$("#interval");
    table.empty();
    table.append(
        "<tr>" +
        "<th>ID de reservation</th> <th>ID du joueur</th> <th>ID de terrain</th> <th>Date</th>" +
        "</tr>"
    );
    var start=""+document.getElementById("startDate").value +" "+ document.getElementById("startTime").value+":00";
    var end=""+document.getElementById("endDate").value +" "+ document.getElementById("endTime").value+":00";
    //$("#interval").text('<?php show_terrains_interval($conn,"'+start+'", "'+end+'"); ?>');
    var filtered= input.filter(function (row) {
        return (Date.parse(row.R_date) >= Date.parse(start)) && (Date.parse(row.R_date) <= Date.parse(end))
    });
    console.log(filtered);
    fillTable(filtered,"interval");
    table=$("#interval tr:nth-child(2)").remove();
}


function tableRowMaker(input){
    let result="<tr>";
    $.each(input,function (key, value) {
        result=result+"<td>"+value+"</td>";
    });

    return result+"</tr>";
}

function tableHeadMaker(input){
    let result="<tr>";
    $.each(input,function (key) {
        result=result+"<th>"+key+"</th>";
    });

    return result+"</tr>";
}

function fillTable(input,tableId){
    let table=$("#"+tableId);
    table.append(tableHeadMaker(input[0]));
    for(i=0;i<input.length;i++){
        table.append(tableRowMaker(input[i]));
    }
}

function fillTableCustom(input,tableId){
    let table=$("#"+tableId);
    for(i=0;i<input.length;i++){
        if(i !== 0) {
            input[i]['supprimer'] = '<input type="radio" name="remove" value="' + input[i]['R_Id'] + '">';
        }else{
            input[i]['supprimer'] = 'Supprimer'
        }
        table.append(tableRowMaker(input[i]));
    }

}

function matrixToTable(matrix,tableId) {
    let result="<table border='border' id='"+tableId+"'>";
    for(i=0;i<matrix.length;i++){
        result += "<tr>";
        for(j=0;j<matrix[i].length;i++){
            result += "<td>"+matrix[i][j]+"</td>";

        }
        result += "</tr>";
    }
    result += "</table>";

    return result;
}