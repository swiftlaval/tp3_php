<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ajouter des joueurs</title>
    <script type="text/javascript" src="indexScript.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="indexStyle.css">
</head>
<body>
<ul>
    <li><a href="adminfo.php">Informations admin</a></li>
    <li><a>Ajout de joueurs</a></li>
    <li><a href="booking.php">Réserver terrain</a></li>
    <li><a href="login.php" onclick="<?php session_destroy(); ?>">Déconnexion</a></li>
</ul>

<?php
/**
 * Created by PhpStorm.
 * User: Tinny
 * Date: 12-Aug-18
 * Time: 11:24 PM
 */

session_start();
include("configdb.php");
include("opendb.php");


if ( isset($_SESSION['user_login'] ) && isset($_SESSION['admin']) ) {

    if(sizeof($_POST)==0) {


        echo "
    <form action='addplayers.php' method='post'>
        <fieldset>
            <legend>Ajout de joueur:</legend>
            
            <label for='nom'>nom </label><input type='text' name='nom' required='required'/><br/>
            <label for='prenom'>prénom </label><input type='text' name='prenom' required='required'/><br/>
            <label for='login'>login </label><input type='text' name='login' required='required'/><br/>
            <label for='mot_de_passe'>mot de passe </label><input type='text' name='mot_de_passe' required='required' minlength='4'/><br/>
            <label for='admin'>donner les privilèges d'admin? </label><input type='checkbox' name='admin' /><br/>
            <input type='submit'/>
        </fieldset>
    </form>
    
    ";
    }else{
        if(add_player($conn,$_POST['nom'],$_POST['prenom'],$_POST['login'],$_POST['mot_de_passe'],isset($_POST['admin']))){
            echo "joueur ajouté avec succès, <a href='addplayers.php'>ajouter un autre?</a>";
        }else{
            echo "login de joueur existant, <a href='addplayers.php'>ressayer?</a>";
        }
    }



    include('closedb.php');
} else {

    header("Location:login.php");
}

?>