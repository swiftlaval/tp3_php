<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login des joueurs et gestionnaires</title>
    <link rel="stylesheet" type="text/css" href="indexStyle.css">
</head>
<body>
<h4>Login des joueurs et gestionnaires:</h4>
<p>TP3 par Moncef Fayjhi. Realisé dans le cadre du cours: IFT3225, Technologies de l'internet</p>
<p>Voici le diagramme de la base de données du TP: </p>
<img alt="diagramme de la base de données" src="https://i.imgur.com/uIv8xko.png"/>

<br/>
<form action="adminfo.php" method="post"><br/>
    <fieldset><br/>
        <div id="error_message"><?php
            if (isset($_GET['message'])) {
                echo urldecode($_GET['message']);
            }
            session_start();
            session_destroy(); ?>
        </div>
        <legend>Bienvenue:</legend>
        <div id="loginstyle">
            <label for="login"> login <input type="text" name="login"/></label>
            <br/><br/>
            <label for="password"> mot de passe <input type="password" name="password"/></label>
            <br/><br/>

            <input type="submit"/>
        </div>
    </fieldset>
</form>
</body>
</html>