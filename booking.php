<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Réservation de terrains</title>
    <link rel="stylesheet" type="text/css" href="indexStyle.css">
    <script type="text/javascript" src="indexScript.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<ul>
<?php
session_start();
if (isset($_SESSION["admin"])) {
    echo '
    <li><a href="adminfo.php">Informations admin</a></li>
    <li><a href="addplayers.php">Ajout de joueurs</a></li>';
}
echo '
    <li><a >Réserver terrain</a></li>
    <li><a href="login.php" ">Déconnexion</a></li>
</ul>
';


include("configdb.php");
include("opendb.php");


if (isset($_SESSION['user_login'])) {

    if (sizeof($_POST) == 0) {


        echo "
    <form action='booking.php' method='post'>
        <fieldset>
            <legend> Réservation de terrain:</legend>
                <label for='idterrain'>Numero de terrain</label>
                <select name='idterrain'>
                
                    <option value='1'>1</option>
                    <option value='2'>2</option>
                    <option value='3'>3</option>
                    <option value='4'>4</option>
                    <option value='5'>5</option>
                    
                    
                </select>
                
                <label for='date'>Date</label>
                <input type=date name='startDate' id='datepicker'/>
                <input type=time name='startTime' min='06:00' max='21:00'/>
                <script>let date = new Date();
                date.setDate(date.getDate() + 1);
                document.getElementById('datepicker').valueAsDate = date;
                </script>
           
            <input type='submit'/>
        </fieldset>
    </form>
    
    ";
        echo "Mes réservations:<form action='booking.php' method='post'>";


        show_player_reservations($conn, $_SESSION['user_login']);
        echo "<input type='submit' value='supprimer'/></form>";
        $array = get_all_reservations($conn);
            echo "   
            <br/>
            Voir les terrains réservés entre deux intervalle de temps:
            <form action='javascript: updateIntervalInfo($array);'>
            
            <input type=date id='startDate'/>
            <input type=time id='startTime'/>
            <br/>
            <input type='date' id='endDate'/>
            <input type='time' id='endTime'/>
            
            <input type='submit'/>
            </form>
            <table border='border' id='interval'></table>
            ";
    } else if (sizeof($_POST) == 3) {

        $p_id = get_player_id_from_login($conn, $_SESSION['user_login']);
        $answer = add_reservation($conn, $p_id, $_POST['idterrain'], $_POST['startDate'], $_POST['startTime']);
        if (1 == $answer) {
            echo "Réservation ajoutée avec succès, <a href='booking.php'>ajouter une autre?</a>";
        } else {
            echo "Réservation refusée, une réservation doit être pour le lendemain, limite de une réservation par jour par personne <a href='booking.php'>ressayer ?</a>";
            echo "code erreur: " . $answer;
        }
    } else if (sizeof($_POST) == 1) {
        if (remove_resrvation($conn, $_POST['remove'])) {
            echo "Réservation retirée";
        } else {
            echo "Erreur, ceci ne devrait jamais arriver ";
        }


    }


    include('closedb.php');
} else {

    header("Location:login.php");
}

?>